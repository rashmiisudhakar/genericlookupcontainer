import {
   LightningElement,
   track,
   api
} from 'lwc';
 
export default class GenericLookupContainer extends LightningElement {
   @track filterfields = 'Id,Phone,Name';
   @track searchKey = 'Name';
   @track objName = 'Account';
   @track fieldlabel = 'Account Name';
   @track isRequired = false;
   @track obj = {};
 
   handleSelection(event){
       if(event.detail.selectedRecord){
           console.log('phone->'+event.detail.selectedRecord.Phone);
       }
   }
}
